#!/bin/bash
APACHE_SOURCE="http://mirror.fibergrid.in/apache/tomcat/tomcat-7/v7.0.70/bin/apache-tomcat-7.0.70.tar.gz"
APACHE_TAR_FILE="apache-tomcat-7.0.70.tar.gz"

echo "Downloading Apache-tomcat"
wget $APACHE_SOURCE
if [ $? -eq 0 ]
then
	echo "Downloaded successfully"
	echo "Extracting the files"
	tar -xvf $APACHE_TAR_FILE
	if [ $? -eq 0 ]
	then
		echo "Extracted successfully"
	else
		echo "Extraction UNsuccessfull"
	fi
	cp WebServiceEmployeeAn.war apache-tomcat-7.0.70/webapps/
    cd apache-tomcat-7.0.70/bin/
    ./startup.sh
    echo "To stop tomcat goto into apache-tomcat-7.0.70/bin and type ./shutdown.sh"
else
	echo "Error in downloading file"
fi
